<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\User */

use app\models\User;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Naujos sąskaitos registracija';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <p>Naujos sąskaitos registravimas. Užpildykite žemiau esančią formą:</p>

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-12 control-label', 'style' => 'text-align: left'],
        ],
    ]); ?>

    <div class="form-group col-lg-12">
        <?= $form->field($model, 'username', ['options' => ['class' => 'col-lg-6']])->textInput(['class' => 'form-control col-lg-12']) ?>
    </div>
    <div class="form-group col-lg-12">
        <?= $form->field($model, 'real_name', ['options' => ['class' => 'col-lg-6']])->textInput() ?>
    </div>
    <div class="form-group col-lg-12">
        <?= $form->field($model, 'email', ['options' => ['class' => 'col-lg-6']])->textInput() ?>
    </div>
    <div class="form-group col-lg-12">
        <?= $form->field($model, 'type', ['options' => ['class' => 'col-lg-6']])->widget(Select2::className(), [
            'data' => User::getTypeName(),
        ]) ?>
    </div>
    <div class="form-group col-lg-12">
        <?= $form->field($model, 'password', ['options' => ['class' => 'col-lg-6']])->passwordInput() ?>
    </div>

    <div class="form-group col-lg-12">
        <div class="col-lg-12">
            <?= Html::submitButton('Užregistruoti', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
