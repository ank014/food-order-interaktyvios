<?php

use app\models\GlobalModel;
use app\models\Item;
use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model Item */

$this->title = 'Veiksmų istorija';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="log_list">

<?php
echo GridView::widget([
    'layout' => '{summary}{items}{pager}',
    'summary' => 'Šiuo metu rodomas <b>{page}</b>-as puslapis (<b>{begin}</b> - <b>{end}</b> įrašai)',
    'dataProvider' => $modelProvider,
    'filterModel' => $model,
    'columns' => [
        [
            'attribute' => 'id',
            'format' => 'raw',
            'filter' => Html::input('input', 'LogModel[id]', (!empty(Yii::$app->request->get()['LogModel']['id'])) ? Yii::$app->request->get()['LogModel']['id'] : null, ['class' => 'form-control', 'placeholder' => 'ID paieška']),
        ],
        [
            'attribute' => 'user_id',
            'format' => 'raw',
            'filter' => Html::input('input', 'LogModel[user_id]', (!empty(Yii::$app->request->get()['LogModel']['user_id'])) ? Yii::$app->request->get()['LogModel']['user_id'] : null, ['class' => 'form-control', 'placeholder' => 'Vartotojo paieška']),
            'value' => function($model) {
                return User::getUserName($model->attributes['user_id']);
            }
        ],
        [
            'attribute' => 'type',
            'format' => 'raw',
            'filter' => Html::activeDropDownList($model, 'type', GlobalModel::getTypeName(), ['class' => 'form-control', 'prompt' => 'Visi tipai']),
            'value' => function($model) {
                return GlobalModel::getTypeName($model->type);
            }
        ],
        [
            'attribute' => 'content',
            'format' => 'raw',
            'filter' => Html::input('input', 'LogModel[content]', (!empty(Yii::$app->request->get()['LogModel']['content'])) ? Yii::$app->request->get()['LogModel']['content'] : null, ['class' => 'form-control', 'placeholder' => 'Turinio paieška']),
        ],
        [
            'attribute' => 'date',
            'format' => 'raw',
            'filter' => Html::input('input', 'LogModel[date]', (!empty(Yii::$app->request->get()['LogModel']['date'])) ? Yii::$app->request->get()['LogModel']['date'] : null, ['class' => 'form-control', 'placeholder' => 'Data']),
            'value' => function($model) {
                return date('Y-m-d', $model->attributes['date']);
            }
        ],
    ],
]);
?>

</div>