<?php

/** @var View $this */
/** @var ActiveForm $form */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = 'Prisijungimas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <div id="login_form">
        <p>Šis tinklapis nėra viešam naudojimui. Prašome prisijungti, įvesdami savo vartotojo vardą ir slaptažodį:</p>

        <?php
        $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-12 control-label'],
            ],
        ]);
        ?>

        <?= $form->field($model, 'username')->textInput() ?>
        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Prisijungti', ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <img src="images/login.png" id="login_image">
</div>
