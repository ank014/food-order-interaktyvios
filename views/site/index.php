<?php

use miloschuman\highcharts\Highcharts;
use yii\web\View;

/** @var View $this */
/** @var array $list Months as string. */
/** @var array $ordersAmount The amount of orders for each month as string. */
/** @var array $specificUserApproved The amount of orders approved for current user. */

$this->title = 'Pagr. Puslapis';
?>

<div class="site-index">
    <div class="jumbotron">
        <h1>Sveiki atvykę, <?php echo Yii::$app->user->identity->username ?>!</h1><br>

        <?php
        echo Highcharts::widget([
            'options' => '{
                "title": { "text": "Užsakymų kiekis per paskutinių 12 mėnesių" },
                "subtitle": { "text": "Bendras užsakymų kiekis ir kiek Jūs patvirtinote jų kiekvieną mėnesį." },
                "legend": "false",
                "xAxis": {
                    "categories": ['.$list.']
                },
                "yAxis": {
                    "title": { "text": "Užsakymų kiekis (vnt.)" }
                },
                "series": [
                    {
                        "type": "areaspline",
                        "name": "Užsakymų",
                        "data": ['.$ordersAmount.']
                    },
                    {
                        "name": "Jūs patvirtinote užsakymų",
                        "data": ['.$specificUserApproved.'],
                        "color": "green"
                    }
                ],
                "plotOptions": {
                    "area": {
                        "fillColor": {
                            "linearGradient": {
                                "x1": 0,
                                "y1": 0,
                                "x2": 0,
                                "y2": 1
                            }
                        }
                    }
                }
            }'
        ]);
        ?>
    </div>
</div>
