<?php

use app\models\GlobalModel;
use app\models\Item;
use app\models\Section;
use app\models\User;
use kartik\editable\Editable;
use kartik\growl\Growl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model Item */

$this->title = 'Prekės peržiūra';
$this->params['breadcrumbs'][] = $this->title;
?>

<h3>Prekės peržiūra: <b><?= $model->name ?></b></h3>

<div id="item_preview">
<?php
$owner = User::getUserName($model->owner, true);

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'id',
            'format' => 'raw',
        ],
        (Yii::$app->user->identity->attributes['type'] > 1 ?
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => Editable::widget([
                'model' => $model,
                'name' => 'Item[name]',
                'asPopover' => true,
                'value' => Html::encode($model->name),
                'preHeader' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ',
                'header' => 'Redaguoti pavadinimą',
                'formOptions' => [
                    'action' => Url::to(['site/change-item', 'id' => $model->id])
                ],
                'editableButtonOptions' => ['class' => 'btn btn-xs btn-default'],
                'submitButton' => [
                    'class' => 'btn btn-xs btn-primary',
                    'icon' => '<i class="glyphicon glyphicon-ok"></i>',
                    'label' => '',
                ],
                'resetButton' => [
                    'class' => 'btn btn-xs btn-default',
                    'icon' => '<i class="glyphicon glyphicon-repeat"></i>',
                    'label' => '',
                ],
                'valueIfNull' => '<i>Nenustatyta</i>',
                'format' => Editable::FORMAT_BUTTON,
                'pluginEvents' => [
                    'editableSuccess' => "function(event, val, form, data) {
                        " . GlobalModel::generateGrowl() . "
                    }"
                ],
            ]),
        ] :
        [
            'attribute' => 'name',
            'format' => 'raw',
        ]),
        (Yii::$app->user->identity->attributes['type'] > 1 ?
        [
            'attribute' => 'section_id',
            'format' => 'raw',
            'value' => Editable::widget([
                'model' => $model,
                'name' => 'Item[section_id]',
                'asPopover' => true,
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data' => $sections,
                'value' => Html::encode(Section::retrieveSectionName($model->section_id)),
                'preHeader' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ',
                'header' => 'Pakeisti skyrių',
                'formOptions' => [
                    'action' => Url::to(['site/change-item', 'id' => $model->id])
                ],
                'editableButtonOptions' => ['class' => 'btn btn-xs btn-default'],
                'submitButton' => [
                    'class' => 'btn btn-xs btn-primary',
                    'icon' => '<i class="glyphicon glyphicon-ok"></i>',
                    'label' => '',
                ],
                'resetButton' => [
                    'class' => 'btn btn-xs btn-default',
                    'icon' => '<i class="glyphicon glyphicon-repeat"></i>',
                    'label' => '',
                ],
                'valueIfNull' => '<i>Nenustatyta</i>',
                'format' => Editable::FORMAT_BUTTON,
                'pluginEvents' => [
                    'editableSuccess' => "function(event, val, form, data) {
                        " . GlobalModel::generateGrowl() . "
                    }"
                ],
            ]),
        ] :
        [
            'attribute' => 'section_id',
            'format' => 'raw',
            'value' => Html::encode(Section::retrieveSectionName($model->section_id)),
        ]),
        (Yii::$app->user->identity->attributes['type'] > 1 ?
        [
            'attribute' => 'amount',
            'format' => 'raw',
            'value' => Editable::widget([
                'model' => $model,
                'name' => 'Item[amount]',
                'asPopover' => true,
                'value' => Html::encode($model->amount),
                'preHeader' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ',
                'header' => 'Redaguoti kiekį',
                'formOptions' => [
                    'action' => Url::to(['site/change-item', 'id' => $model->id])
                ],
                'editableButtonOptions' => ['class' => 'btn btn-xs btn-default'],
                'submitButton' => [
                    'class' => 'btn btn-xs btn-primary',
                    'icon' => '<i class="glyphicon glyphicon-ok"></i>',
                    'label' => '',
                ],
                'resetButton' => [
                    'class' => 'btn btn-xs btn-default',
                    'icon' => '<i class="glyphicon glyphicon-repeat"></i>',
                    'label' => '',
                ],
                'valueIfNull' => '<i>Nenustatyta</i>',
                'format' => Editable::FORMAT_BUTTON,
                'pluginEvents' => [
                    'editableSuccess' => "function(event, val, form, data) {
                        " . GlobalModel::generateGrowl() . "
                    }"
                ],
            ]),
        ] :
        [
            'attribute' => 'amount',
            'format' => 'raw',
        ]),
        [
            'attribute' => 'owner',
            'format' => 'raw',
            'value' => Html::a(Html::encode($owner['username']), ['site/user-view', 'id' => $owner['id']]),
        ],
        [
            'attribute' => 'created_at',
            'format' => 'raw',
            'value' => Html::encode(date('Y-m-d h:m:s', $model->created_at)),
        ],
    ],
]);

echo Html::a(Html::button('Grįžti į prekių sąrašą', ['class' => 'btn btn-info']), Url::to(['site/item-list'])).' ';

if (Yii::$app->user->identity->attributes['type'] > 1)
    echo Html::a(Html::button('Pašalinti šią prekę', ['class' => 'btn btn-danger']), Url::to(['site/item-remove', 'id' => $model->id]));
?>
</div>