<?php

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var ActiveForm $form */
/** @var User $model */

$this->title = 'Naujos sąskaitos registracija';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <p>Naujos sąskaitos registravimas. Užpildykite žemiau esančią formą:</p>

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-12 control-label', 'style' => 'text-align: left'],
        ],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['site/user-validation']),
    ]); ?>

    <div class="form-group col-lg-12">
        <?= $form->field($model, 'username', ['options' => ['class' => 'col-lg-6']])->textInput(['class' => 'form-control col-lg-12']) ?>
    </div>
    <div class="form-group col-lg-12">
        <?= $form->field($model, 'real_name', ['options' => ['class' => 'col-lg-6']])->textInput() ?>
    </div>
    <div class="form-group col-lg-12">
        <?= $form->field($model, 'email', ['options' => ['class' => 'col-lg-6']])->textInput() ?>
    </div>
    <div class="form-group col-lg-12">
        <?= $form->field($model, 'password', ['options' => ['class' => 'col-lg-6']])->passwordInput() ?>
    </div>

    <div class="form-group col-lg-12">
        <div class="col-lg-12">
            <?= Html::submitButton('Užsiregistruoti', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
