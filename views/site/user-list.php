<?php

use app\models\User;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var ActiveForm $form */
/** @var User $model */
/** @var ActiveDataProvider $modelProvider */

$this->title = 'Vartotojų sąrašas';
$this->params['breadcrumbs'][] = $this->title;

//Pjax::begin(['enablePushState' => true, 'enableReplaceState' => false, 'timeout' => 10000]);
echo GridView::widget([
    'tableOptions' => [
        'class' => 'linked-row',
    ],
    'resizableColumns' => false,
    'caption' => '<div class="table-heading">Vartotojai</div><div class="header-buttons-right"></div>',
    'layout' => '{items}<div class="footer"><div class="footer-pagination">{pager}</div>{summary}</div>',
    'dataProvider' => $modelProvider,
    'filterModel' => $model,
    'columns' => [
        [
            'attribute' => 'id',
            'format' => 'raw',
            'filter' => Html::input('input', 'User[id]', $model->id, ['class' => 'form-control', 'placeholder' => 'ID paieška']),
        ],
        [
            'attribute' => 'username',
            'format' => 'raw',
            'filter' => Html::input('input', 'User[username]', $model->username, ['class' => 'form-control', 'placeholder' => 'Vardo paieška']),
        ],
        [
            'attribute' => 'real_name',
            'format' => 'raw',
            'filter' => Html::input('input', 'User[real_name]', $model->real_name, ['class' => 'form-control', 'placeholder' => 'Tikrojo vardo paieška']),
        ],
        [
            'attribute' => 'type',
            'format' => 'raw',
            'filter' => Select2::widget([
                'name' => 'User[type]',
                'data' => User::getTypeName(true),
                'hideSearch' => true,
                'value' => $model['type'],
            ]),
            'value' => function($model) {
                return User::getTypeName()[$model->type];
            }
        ],
        [
            'attribute' => 'email',
            'format' => 'raw',
            'filter' => Html::input('input', 'User[email]', $model->email, ['class' => 'form-control', 'placeholder' => 'El. pašto paieška']),
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'edit-col'],
            'value' => function($model) {
                return
                    Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['user-view', 'id' => $model->id]), [
                        'title' => 'Redaguoti',
                        'data-toggle' => 'tooltip',
                    ]);
            },
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'edit-col'],
            'value' => function($model) {
                return Html::a('<span class="glyphicon glyphicon-trash removalButton"></span>', '#', [
                    'title' => 'Ištrinti',
                    'data-toggle' => 'tooltip',
                    'data-id' => $model->id,
                ]);
            },
        ]
    ],
]);
//Pjax::end();

echo Html::a(Html::button('Pridėti naują vartotoją', ['class' => 'btn btn-info']), Url::to(['user-admin-register']));

$this->registerJs("
    var targetURL = '".Url::to(['user-remove'])."';
", $this::POS_END);