<?php

use app\models\GlobalModel;
use app\models\User;
use kartik\editable\Editable;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;

$this->title = 'Vartotojo peržiūra';
$this->params['breadcrumbs'][] = $this->title;

/** @var View $this */
/** @var User $model */
?>

<h2>Vartotojo <b><?= $model->username ?></b> peržiūra</h2>

<div id="user_preview">
<?php
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'id',
            'format' => 'raw',
        ],
        [
            'attribute' => 'username',
            'format' => 'raw',
            'value' => Editable::widget([
                'model' => $model,
                'name' => 'User[username]',
                'asPopover' => true,
                'value' => Html::encode($model->username),
                'preHeader' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ',
                'header' => 'Redaguoti prisijungimo vardą',
                'formOptions' => [
                    'action' => Url::to(['site/change-user', 'id' => $model->id])
                ],
                'submitButton' => [
                    'class' => 'btn btn-xs btn-info',
                    'icon' => '<i class="glyphicon glyphicon-ok"></i> Patvirtinti',
                ],
                'resetButton' => [
                    'class' => 'btn btn-xs btn-default',
                    'icon' => '<i class="glyphicon glyphicon-repeat"></i> Atstatyti',
                ],
                'valueIfNull' => '<i>Nenustatyta</i>',
                'pluginEvents' => [
                    'editableSuccess' => "function() {
                        ".GlobalModel::generateGrowl()."
                    }"
                ],
            ]),
        ],
        [
            'attribute' => 'real_name',
            'format' => 'raw',
            'value' => Editable::widget([
                'model' => $model,
                'name' => 'User[real_name]',
                'asPopover' => true,
                'value' => Html::encode($model->real_name),
                'preHeader' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ',
                'header' => 'Redaguoti prisijungimo vardą',
                'formOptions' => [
                    'action' => Url::to(['site/change-user', 'id' => $model->id])
                ],
                'submitButton' => [
                    'class' => 'btn btn-xs btn-info',
                    'icon' => '<i class="glyphicon glyphicon-ok"></i> Patvirtinti',
                ],
                'resetButton' => [
                    'class' => 'btn btn-xs btn-default',
                    'icon' => '<i class="glyphicon glyphicon-repeat"></i> Atstatyti',
                ],
                'valueIfNull' => '<i>Nenustatyta</i>',
                'pluginEvents' => [
                    'editableSuccess' => "function() {
                        ".GlobalModel::generateGrowl()."
                    }"
                ],
            ]),
        ],
        [
            'attribute' => 'email',
            'format' => 'raw',
            'value' => Editable::widget([
                'model' => $model,
                'name' => 'User[email]',
                'asPopover' => true,
                'value' => Html::encode($model->email),
                'preHeader' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ',
                'header' => 'Redaguoti el. paštą',
                'formOptions' => [
                    'action' => Url::to(['site/change-user', 'id' => $model->id])
                ],
                'submitButton' => [
                    'class' => 'btn btn-xs btn-info',
                    'icon' => '<i class="glyphicon glyphicon-ok"></i> Patvirtinti',
                ],
                'resetButton' => [
                    'class' => 'btn btn-xs btn-default',
                    'icon' => '<i class="glyphicon glyphicon-repeat"></i> Atstatyti',
                ],
                'valueIfNull' => '<i>Nenustatyta</i>',
                'pluginEvents' => [
                    'editableSuccess' => "function() {
                        ".GlobalModel::generateGrowl()."
                    }"
                ],
            ]),
        ],
        [
            'attribute' => 'type',
            'format' => 'raw',
            'value' => Editable::widget([
                'model' => $model,
                'name' => 'User[type]',
                'asPopover' => true,
                'value' => Html::encode($model->type),
                'displayValue' => Html::encode(User::getTypeName()[$model->type]),
                'preHeader' => '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ',
                'header' => 'Redaguoti tipą',
                'formOptions' => [
                    'action' => Url::to(['site/change-user', 'id' => $model->id])
                ],
                'submitButton' => [
                    'class' => 'btn btn-xs btn-info',
                    'icon' => '<i class="glyphicon glyphicon-ok"></i> Patvirtinti',
                    'label' => '',
                ],
                'resetButton' => [
                    'class' => 'btn btn-xs btn-default',
                    'icon' => '<i class="glyphicon glyphicon-repeat"></i> Atstatyti',
                    'label' => '',
                ],
                'valueIfNull' => '<i>Nenustatyta</i>',
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data' => User::getTypeName(),
                'pluginEvents' => [
                    'editableSuccess' => "function() {
                        ".GlobalModel::generateGrowl()."
                    }"
                ],
            ]),
        ],
    ]
]);

echo Html::a(Html::button('Grįžti į vartotojų sąrašą', ['class' => 'btn btn-info']), Url::to(['user-list']));
echo ' ';
echo Html::a(Html::button('Pašalinti šį vartotoją', ['class' => 'btn btn-danger']), Url::to(['user-remove', 'id' => $model->id]));
?>
</div>