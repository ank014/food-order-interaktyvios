<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

if (Yii::$app->errorHandler->exception->statusCode == 403) {
    $this->title = 'Leidimas neduotas';
} elseif (Yii::$app->errorHandler->exception->statusCode == 404) {
    $this->title = $message = 'Puslapis nerastas';
} else {
    $this->title = $message = 'Serverio klaida';
}
?>

<div class="site-error">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>Vykdant Jūsų užklausą, įvyko klaida. Pabandykite grįžti atgal ir pakartoti.</p>

    <button class="btn btn-info" onclick="window.history.back()">Grįžti atgal</button><br><br><br>

    <?php if (Yii::$app->errorHandler->exception->statusCode == 403): ?>
        <img src="../web/images/403-Page-Forbidden.png" style="width: 45%">
    <?php elseif (Yii::$app->errorHandler->exception->statusCode == 404): ?>
        <img src="../web/images/404-Page-Not-Found.png">
    <?php else: ?>
        <img src="../web/images/500-Internal-Error.png" style="width: 17%">
    <?php endif; ?>
</div>
