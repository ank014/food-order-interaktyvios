<?php

use app\models\Category;
use app\models\Meal;

/** @var Category $model */
?>

<div class="menu_info_detailed">
    <h2><?php echo $model->name ?></h2>

    <?php
    if ($model->parent_id == 0) {
        if (count(Category::findAll(['parent_id' => $model->id])) == 0) {
            echo '<i>Ši šakninė kategorija neturi priskirtų subkategorijų.</i>';
        } else {
            echo '<h4>Priskirtos subkategorijos:</h4>';
        }
    } else {
        if (count(Meal::findAll(['category' => $model->id])) == 0) {
            echo '<i>Ši subkategorija neturi priskirtų patiekalų.</i>';
        } else {
            echo '<h4>Priskirti patiekalai:</h4>';
        }
    }
    ?>

    <ul>
        <?php
        if ($model->parent_id == 0) {
            $subcats = Category::findAll(['parent_id' => $model->id]);

            foreach ($subcats as $subcat) {
                echo '<li>'.$subcat['name'].'</li>';
            }
        } else {
            $meals = Meal::findAll(['category' => $model->id]);

            foreach ($meals as $meal) {
                echo '<li>'.$meal['name'].'</li>';
            }
        }
        ?>
    </ul>

    <h2>Aprašymas</h2>
    <?php echo $model->description ?>
</div>

