<?php

use app\models\Category;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var ActiveForm $form */

$this->title = 'Naujos kategorijos pridėjimas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="menu_div">
    <div id="menu_form">
        <?php
        $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['meal/menu-validation']),
        ]);

        echo $form->field($model, 'name', ['options' => ['class' => 'col-lg-6']])->textInput();

        echo $form->field($model, 'parent_id', ['options' => ['class' => 'col-lg-6']])->widget(Select2::className(), [
            'data' => array_merge(['' => [0 => 'Tai yra tėvinė kategorija']], Category::getCombinedCategories()),
        ]);

        echo $form->field($model, 'description', ['options' => ['class' => 'col-lg-12']])->textarea(['rows' => 3]);
        ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?php
                if (!is_null($model->id)) {
                    echo Html::submitButton('Atnaujinti kategorijos duomenis', ['class' => 'btn btn-info']);
                } else {
                    echo Html::submitButton('Pridėti naują kategoriją', ['class' => 'btn btn-info']);
                }
                ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
