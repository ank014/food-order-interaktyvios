<?php

use app\models\Category;
use app\models\GlobalModel;
use app\models\Meal;
use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\grid\ExpandRowColumn;
use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var Category $model */
/** @var ActiveDataProvider $dataProvider */
/** @var ActiveDataProvider $dataProviderRootOnly */

$this->title = 'Patiekalai';
$this->params['breadcrumbs'][] = $this->title;

echo GridView::widget([
    'filterModel' => $model,
    'tableOptions' => [
        'class' => 'linked-row',
    ],
    'resizableColumns' => false,
    'caption' => '<div class="table-heading">Šakninės (pagrindinės) kategorijos</div><div class="header-buttons-right"></div>',
    'layout' => '{items}<div class="footer"><div class="footer-pagination">{pager}</div>{summary}</div>',
    'dataProvider' => $dataProvider,
    'summary' => false,
    'filterRowOptions' => [
        'style' => 'text-align: center;',
    ],
    'columns' => [
        [
            'attribute' => '',
            'format' => 'raw',
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'class' => ExpandRowColumn::className(),
            'value' => function() {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function($model) {
                return Yii::$app->controller->renderPartial('_menu-info', ['model' => $model]);
            },
            'expandIcon' => Icon::show('info-circle'),
            'collapseIcon' => Icon::show('level-up'),
            'expandTitle' => 'Peržiūrėti detalesnę informaciją',
            'collapseTitle' => 'Suskleisti detalesnę informaciją',
            'expandAllTitle' => 'Išskleisti matomų patiekalų detalesnę informaciją',
            'collapseAllTitle' => 'Suskleisti matomų patiekalų detalesnę informaciją',
        ],
        [
            'attribute' => 'id',
            'format' => 'raw',
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'filter' => false,
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
        ],
        [
            'attribute' => '',
            'label' => 'Subkategorijų kiekis',
            'format' => 'raw',
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'value' => function($model) {
                return count(Category::findAll(['parent_id' => $model->id]));
            },
        ],
        [
            'attribute' => 'description',
            'format' => 'raw',
            'filter' => false,
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'value' => function($model) {
                return (strlen($model->description) > 80 ? substr($model->description, 0, 80).'...' : $model->description);
            },
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'label' => 'Peržiūrėti',
            'contentOptions' => ['class' => 'edit-col', 'style' => 'width: 5%'],
            'value' => function($model) {
                return
                    Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['meal/menu-view', 'id' => $model->id]), [
                        'title' => 'Peržiūrėti',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model->id,
                    ]);
            },
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'label' => 'Ištrinti',
            'contentOptions' => ['class' => 'edit-col', 'style' => 'width: 5%'],
            'value' => function($model) {
                if (count(Category::findAll(['parent_id' => $model->id])) > 0) {
                    return '';
                }

                return
                    Html::a('<span class="glyphicon glyphicon-remove-sign removalButton"></span>', '#', [
                        'title' => 'Ištrinti',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model->id,
                    ]);
            },
        ],
    ],
    'export' => false,
]);

echo GridView::widget([
    'filterModel' => $model,
    'tableOptions' => [
        'class' => 'linked-row',
    ],
    'resizableColumns' => false,
    'caption' => '<div class="table-heading">Subkategorijos</div><div class="header-buttons-right"></div>',
    'layout' => '{items}<div class="footer"><div class="footer-pagination">{pager}</div>{summary}</div>',
    'dataProvider' => $dataProviderRootOnly,
    'summary' => false,
    'filterRowOptions' => [
        'style' => 'text-align: center;',
    ],
    'columns' => [
        [
            'attribute' => '',
            'format' => 'raw',
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'class' => ExpandRowColumn::className(),
            'value' => function() {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function($model) {
                return Yii::$app->controller->renderPartial('_menu-info', ['model' => $model]);
            },
            'expandIcon' => Icon::show('info-circle'),
            'collapseIcon' => Icon::show('level-up'),
            'expandTitle' => 'Peržiūrėti detalesnę informaciją',
            'collapseTitle' => 'Suskleisti detalesnę informaciją',
            'expandAllTitle' => 'Išskleisti matomų patiekalų detalesnę informaciją',
            'collapseAllTitle' => 'Suskleisti matomų patiekalų detalesnę informaciją',
        ],
        [
            'attribute' => 'id',
            'format' => 'raw',
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'filter' => false,
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'value' => function($model) {
                return $model->name;
            },
        ],
        [
            'attribute' => 'parent_id',
            'format' => 'raw',
            'filter' => false,
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'value' => function($model) {
                return Category::findOne(['id' => $model->parent_id])['name'];
            },
        ],
        [
            'attribute' => '',
            'label' => 'Patiekalų kiekis',
            'format' => 'raw',
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'value' => function($model) {
                return count(Meal::findAll(['category' => $model->id]));
            }
        ],
        [
            'attribute' => 'description',
            'format' => 'raw',
            'filter' => false,
            'headerOptions' => [
                'style' => 'text-align: center',
            ],
            'contentOptions' => [
                'style' => 'text-align: center',
            ],
            'value' => function($model) {
                return (strlen($model->description) > 80 ? substr($model->description, 0, 80).'...' : $model->description);
            },
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'label' => 'Peržiūrėti',
            'contentOptions' => ['class' => 'edit-col', 'style' => 'width: 5%'],
            'value' => function($model) {
                return
                    Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['meal/menu-view', 'id' => $model->id]), [
                        'title' => 'Peržiūrėti',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model->id,
                    ]);
            },
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'label' => 'Ištrinti',
            'contentOptions' => ['class' => 'edit-col', 'style' => 'width: 5%'],
            'value' => function($model) {
                if (count(Meal::findAll(['category' => $model->id])) > 0) {
                    return '';
                }

                return Html::a('<span class="glyphicon glyphicon-remove-sign removalButton"></span>', '#', [
                    'title' => 'Ištrinti',
                    'data-toggle' => 'tooltip',
                    'data-id' => $model->id,
                ]);
            },
        ],
    ],
    'export' => false,
]);

echo Html::a(Html::button('Pridėti naują kategoriją', ['class' => 'btn btn-info']), Url::to(['menu-add']));

$this->registerJs("
    var targetURL = '".Url::to(['menu-remove'])."';
", $this::POS_END);