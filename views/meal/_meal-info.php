<?php

use app\models\Category;
use app\models\DishOrder;
use app\models\Meal;
use yii\bootstrap\Html;

/** @var Meal $model */
?>

<div class="meal_info_detailed">
    <?php echo Html::img('../uploads/'.$model->image, ['style' => 'max-width: 100%']); ?>
</div>

<div class="meal_info_detailed">
    <h2><?php echo $model->name ?></h2>
    <?php echo Html::tag('p', '<i>'.$model->description.'</i>', ['style' => 'margin: 1em 0']); ?>
    <?php echo '<br>Kategorija: <b>'.Category::getCategoryChain($model->id).'</b><br>'; ?>
    <?php echo ($model->is_available ? 'Užsisakyti šį patiekalą <font color="green">galima</font>.' :
        'Užsisakyti šio patiekalo <font color="red">negalima</font>.');
    ?>
    <?php echo '<br>Kaina už vieną porciją/vienetą: <b>'.$model->price.'€</b>'; ?>
    <?php echo '<br>Užsakymų kiekis, kuriuose buvo pasirinktas šis patiekalas: <b>'.DishOrder::getAmountFromMealID($model->id).'</b> vnt.'; ?>
</div>

