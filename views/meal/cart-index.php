<?php

use yii\web\ForbiddenHttpException;
use yii\web\View;

/** @var View $this */

throw new ForbiddenHttpException('Ši funkcija šiuo metu nėra sukurta.');

$this->title = 'Krepšelis';
$this->params['breadcrumbs'][] = $this->title;

