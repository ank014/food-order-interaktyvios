<?php

use app\models\Category;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var ActiveForm $form */

$this->title = 'Naujo patiekalo pridėjimas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="meal_div">
    <div id="meal_form">
        <?php
        $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal', ['enctype' => 'multipart/form-data']],
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['meal/meal-validation']),
        ]);

        echo $form->field($model, 'name', ['options' => ['class' => 'col-lg-6']])->textInput();

        echo $form->field($model, 'category', ['options' => ['class' => 'col-lg-6']])->widget(Select2::className(), [
            'data' => Category::getCategoryListAsDropdown(),
        ]);

        echo $form->field($model, 'description', ['options' => ['class' => 'col-lg-12']])->textarea(['rows' => 3]);

        echo $form->field($model, 'image', ['options' => ['class' => 'col-lg-12']])->fileInput([
            'accept' => 'image/*',
        ])->hint((!is_null($model->id) ? 'Jei nepasirinktas naujas failas, bus paliktas senasis.' : ''));

        if (!is_null($model->id)) {
            echo Html::beginTag('div', ['class' => 'col-lg-12']);
                echo Html::checkbox('removeOldImage', false, ['label' => 'Pašalinti senąjį failą']);
            echo Html::endTag('div');
        };

        echo $form->field($model, 'price', ['options' => ['class' => 'col-lg-6']]);

        echo $form->field($model, 'is_available', ['options' => ['class' => 'col-lg-6']])->dropDownList(
            ['Neleidžiama', 'Leidžiama'], ['value' => (!is_null($model->id) ? $model->is_available : 1)]
        );
        ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?php
                if (!is_null($model->id)) {
                    echo Html::submitButton('Atnaujinti patiekalo duomenis', ['class' => 'btn btn-info']);
                } else {
                    echo Html::submitButton('Pridėti naują patiekalą', ['class' => 'btn btn-info']);
                }
                ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
