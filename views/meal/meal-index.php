<?php

use app\models\Category;
use app\models\Meal;
use kartik\grid\ExpandRowColumn;
use kartik\grid\GridView;
use kartik\icons\Icon;
use kartik\select2\Select2;
use kartik\slider\Slider;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var Meal $model */
/** @var ActiveDataProvider $dataProvider */
/** @var array $categoriesList */
/** @var integer $priceMin */
/** @var integer $priceMax */

$this->title = 'Patiekalai';
$this->params['breadcrumbs'][] = $this->title;

if (empty(Yii::$app->request->get('Meal'))) {
    $dish = [];
} else {
    $dish = Yii::$app->request->get('Meal');
}

if (array_key_exists('price', $dish)) {
    $priceCurrentMin = explode(',', $dish['price'])[0];
    $priceCurrentMax = explode(',', $dish['price'])[1];
} else {
    $priceCurrentMin = 0;
    $priceCurrentMax = 50;
}

//Pjax::begin(['enablePushState' => true, 'enableReplaceState' => false, 'timeout' => 10000]);
echo GridView::widget([
    'filterModel' => $model,
    'tableOptions' => [
        'class' => 'linked-row',
    ],
    'resizableColumns' => false,
    'caption' => '<div class="table-heading">Patiekalai</div><div class="header-buttons-right"></div>',
    'layout' => '{items}<div class="footer"><div class="footer-pagination">{pager}</div>{summary}</div>',
    'dataProvider' => $dataProvider,
    'summary' => false,
    'filterRowOptions' => [
        'style' => 'text-align: center;'
    ],
    'columns' => [
        [
            'attribute' => '',
            'format' => 'raw',
            'class' => ExpandRowColumn::className(),
            'value' => function() {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function($model) {
                return Yii::$app->controller->renderPartial('_meal-info', ['model' => $model]);
            },
            'expandIcon' => Icon::show('info-circle'),
            'collapseIcon' => Icon::show('level-up'),
            'expandTitle' => 'Peržiūrėti detalesnę informaciją',
            'collapseTitle' => 'Suskleisti detalesnę informaciją',
            'expandAllTitle' => 'Išskleisti matomų patiekalų detalesnę informaciją',
            'collapseAllTitle' => 'Suskleisti matomų patiekalų detalesnę informaciją',
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return $model->name;
            }
        ],
        [
            'attribute' => 'category',
            'format' => 'raw',
            'filter' => Select2::widget([
                'name' => 'Meal[category]',
                'data' => Category::getCombinedCategories(),
                'value' => (array_key_exists('category', $dish) ? $dish['category'] : null),
                'options' => [
                    'prompt' => 'Visos kategorijos',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return $model->category0->name;
            }
        ],
        [
            'attribute' => 'price',
            'format' => 'raw',
            'filter' => Slider::widget([
                'name' => 'Meal[price]',
                'value' => $priceCurrentMin.','.$priceCurrentMax,
                'sliderColor' => Slider::TYPE_PRIMARY,
                'pluginOptions' => [
                    'min' => $priceMin,
                    'max' => $priceMax,
                    'step' => 0.5,
                    'range' => true,
                ],
            ]),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return $model->price;
            }
        ],
        [
            'attribute' => 'is_available',
            'format' => 'raw',
            'filter' => Select2::widget([
                'name' => 'Meal[is_available]',
                'data' => [1 => 'Tik galimus užsisakyti', 0 => 'Įtraukti ir nepasiekiamus'],
                'hideSearch' => true,
                'value' => (array_key_exists('is_available', $dish) ? $dish['is_available'] : 0),
            ]),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return $model->is_available == 0 ? 'Neleidžiama' : 'Leidžiama užsisakyti';
            }
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'edit-col'],
            'value' => function($model) {
                return
                    Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['meal-view', 'id' => $model->id]), [
                        'title' => 'Peržiūrėti/redaguoti',
                        'data-toggle' => 'tooltip',
                    ]);
            },
            'visible' => (Yii::$app->user->identity->type >= 2),
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'edit-col'],
            'value' => function($model) {
                return
                    Html::a('<span class="glyphicon glyphicon-random"></span>', Url::to(['meal-change-status', 'id' => $model->id]), [
                        'title' => 'Pakeisti statusą į '.($model->is_available ? 'nepasiekiamą' : 'pasiekiamą'),
                        'data-toggle' => 'tooltip',
                    ]);
            },
            'visible' => (Yii::$app->user->identity->type >= 2),
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'edit-col'],
            'value' => function($model) {
                return
                    Html::a('<span class="glyphicon glyphicon-remove-sign removalButton"></span>', '#', [
                        'title' => 'Ištrinti',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model->id,
                    ]);
            },
            'visible' => (Yii::$app->user->identity->type >= 2),
        ]
    ],
    'export' => false,
]);
//Pjax::end();

if (Yii::$app->user->identity->type >= 2) {
    echo Html::a(Html::button('Pridėti naują patiekalą', ['class' => 'btn btn-info']), Url::to(['meal-add']));
}

$this->registerJs("
    var targetURL = '".Url::to(['meal-remove'])."';
", $this::POS_END);