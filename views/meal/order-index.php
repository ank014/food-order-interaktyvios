<?php

use app\models\DishOrder;
use app\models\User;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var DishOrder $model */
/** @var ActiveDataProvider $dataProvider */

$this->title = 'Užsakymai';
$this->params['breadcrumbs'][] = $this->title;

//Pjax::begin(['enablePushState' => true, 'enableReplaceState' => false, 'timeout' => 10000]);
echo GridView::widget([
    'filterModel' => $model,
    'tableOptions' => [
        'class' => 'linked-row',
    ],
    'resizableColumns' => false,
    'caption' => '<div class="table-heading">Patiekalai</div><div class="header-buttons-right"></div>',
    'layout' => '{items}<div class="footer"><div class="footer-pagination">{pager}</div>{summary}</div>',
    'dataProvider' => $dataProvider,
    'summary' => false,
    'filterRowOptions' => [
        'style' => 'text-align: center;'
    ],
    'columns' => [
        [
            'attribute' => 'dish_id',
            'format' => 'raw',
            'filter' => Html::input('text', 'DishOrder[dish_id]', $model->dish_id, ['class' => 'form-control']),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return $model->dish->name;
            }
        ],
        [
            'attribute' => 'customer_id',
            'format' => 'raw',
            'filter' => Select2::widget([
                'name' => 'DishOrder[customer_id]',
                'data' => User::getAllUsersAsDropdown(true),
                'hideSearch' => true,
                'value' => $model->customer_id,
            ]),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return $model->customer->real_name;
            }
        ],
        [
            'attribute' => 'address',
            'format' => 'raw',
            'filter' => Html::input('text', 'DishOrder[address]', $model->address, ['class' => 'form-control']),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return $model->address;
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'filter' => Select2::widget([
                'name' => 'DishOrder[status]',
                'data' => DishOrder::getOrderTypes(),
                'hideSearch' => true,
                'value' => $model->status,
            ]),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return DishOrder::getOrderTypes()[$model->status];
            },
        ],
        [
            'attribute' => 'approver_id',
            'format' => 'raw',
            'filter' => Select2::widget([
                'name' => 'DishOrder[approver_id]',
                'data' => User::getAllUsersAsDropdown(true),
                'hideSearch' => true,
                'value' => $model->approver_id,
            ]),
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                if (!is_null($model->approver)) {
                    return $model->approver->real_name;
                } else {
                    return '';
                }
            }
        ],
        [
            'attribute' => 'time',
            'label' => 'Užsakymo laikas',
            'format' => 'raw',
            'filter' => false,
            'headerOptions' => [
                'style' => 'text-align: center;',
            ],
            'contentOptions' => [
                'style' => 'text-align: center;',
            ],
            'value' => function($model) {
                return date('Y-m-d H:i', $model->time);
            }
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'edit-col'],
            'value' => function($model) {
                if ($model->status == 1) {
                    return
                        Html::a('<span class="glyphicon glyphicon-ok-circle confirmationButton"></span>', '#', [
                            'title' => 'Patvirtinti užsakymą',
                            'data-toggle' => 'tooltip',
                            'data-id' => $model->id,
                            'data-url' => 'approve',
                        ]);
                } else {
                    return '';
                }
            },
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'contentOptions' => ['class' => 'edit-col'],
            'value' => function($model) {
                if ($model->status == 1) {
                return
                    Html::a('<span class="glyphicon glyphicon-ban-circle confirmationButton"></span>', '#', [
                        'title' => 'Atmesti užsakymą',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model->id,
                        'data-url' => 'decline',
                    ]);
                } else {
                    return '';
                }
            },
        ]
    ],
    'export' => false,
]);
//Pjax::end();

$this->registerJsFile('order.js');
$this->registerJs("
    var targetURL = '".Url::to(['order-'])."';
", $this::POS_END);