<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

Modal::begin([
    'id' => 'confirm_modal',
    'header' => Html::tag('h3', 'Reikalingas patvirtinimas', ['class' => 'modal-title']),
    'footer' => '',
    'size' => Modal::SIZE_LARGE,
]);
?>

<div class="col-xs-12">
    <p style="text-align: center">Patvirtinkite savo veiksmą, paspausdami mygtuką „patvirtinti".</p>
    <div class="text-center">
        <?= Html::a(Html::button('Patvirtinti', ['class' => 'btn btn-info']), null, ['id' => 'confirm_modal_button']) ?>
    </div>
</div>

<?php
Modal::end();