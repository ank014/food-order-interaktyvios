<?php

use app\models\Category;
use app\models\User;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/** @var View $this */
/** @var $content string */
/** @var array $categoryList */

AppAsset::register($this);
$this->beginPage();
date_default_timezone_set('Europe/Vilnius');

$categories = Category::getRootCategories();

for ($i = 0; $i < count($categories); $i++) {
    $categoryList[$i] = [
        'label' => FA::icon('coffee').' '.$categories[$i]['name'].' ('.count($categories[$i]['dishes']).')',
        'url' => Url::to(['/meal/meal-index', 'Meal[category]' => $categories[$i]['id']])
    ];
}

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->params['siteTitle'] ?></title>
    <link rel="shortcut icon" href="../web/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../web/images/favicon.ico" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body>
<?php
$this->beginBody();
NavBar::begin();
NavBar::end();

$user = User::getUserDetails(Yii::$app->user->id);
?>

<div id="main_site_page" class="wrap">
    <div id="main_header_menu">
        <?php if (!Yii::$app->user->isGuest): ?>
            <div id="main_header_div"><button class="btn main_header_button">Prisijungęs vartotojas:<br><?php echo $user['real_name'].' ('.$user['email'].')' ?></button></div>
        <?php endif; ?>
    </div>
    <div id="main_side_menu" class="nav-side-menu">
        <div class="menu-list">
            <a href="<?php echo Url::to(['/site/index']) ?>"><img src="../web/images/logo-2958.png" class="brand" alt="Logo"></a>
            <?php
            echo '<div id="menu_ul">';
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'encodeLabels' => false,
                'items' => [
                    Html::tag('span', '<br>Pagrindiniai veiksmai', ['class' => 'menu_title']),
                    [
                        'label' => FA::icon('home').'Namai',
                        'url' => ['/site/index'],
                        'options' => [
                            'class' => (Yii::$app->controller->action->id == 'index' ? 'highlighted' : ''),
                        ],
                    ],
                    [
                        'label' => FA::icon('cutlery').'Patiekalai',
                        'url' => ['/meal/meal-index'],
                        'visible' => !Yii::$app->user->isGuest,
                        'options' => [
                            'class' => (Yii::$app->controller->action->id == 'meal-index' ? 'highlighted' : ''),
                        ],
                    ],
                    (!Yii::$app->user->isGuest && Yii::$app->user->identity->type > 1 ?
                        Html::tag('span', '<br>Administravimas', ['class' => 'menu_title']) :
                    ''),
                    [
                        'label' => FA::icon('book').'Meniu',
                        'url' => ['/meal/menu-index'],
                        'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->type > 1,
                        'options' => [
                            'class' => (Yii::$app->controller->action->id == 'menu-index' ? 'highlighted' : ''),
                        ],
                    ],
                    [
                        'label' => FA::icon('credit-card').'Užsakymai',
                        'url' => ['/meal/order-index', 'DishOrder[status]' => 1],
                        'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->type > 1,
                        'options' => [
                            'class' => (Yii::$app->controller->action->id == 'order-index' ? 'highlighted' : ''),
                        ],
                    ],
                    [
                        'label' => FA::icon('user-circle').'Vartotojai',
                        'url' => ['/site/user-list'],
                        'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->type == 3,
                        'options' => [
                            'class' => (Yii::$app->controller->action->id == 'user-list' ? 'highlighted' : ''),
                        ],
                    ],
                    Html::tag('span', '<br>Vartotojo veiksmai', ['class' => 'menu_title']),
                    [
                        'label' => FA::icon('sign-in').'Prisijungimas',
                        'url' => '#',
                        'visible' => Yii::$app->user->isGuest,
                        'options' => [
                            'id' => 'menu_login',
                        ],
                    ],
                    [
                        'label' => FA::icon('user-plus').'Registracija',
                        'url' => '#',
                        'visible' => Yii::$app->user->isGuest,
                        'options' => [
                            'id' => 'menu_register',
                        ],
                    ],
                    [
                        'label' => FA::icon('sign-out').'Atsijungti',
                        'url' => ['/site/user-logout'],
                        'visible' => !Yii::$app->user->isGuest,
                        'options' => [
                        ],
                    ],
                ],
            ]);
            echo '</div>';

            echo Html::tag('p', '&copy; '.Yii::$app->params['siteTitle'], ['class' => 'footer']);
            echo Html::tag('p', 'Serverio laikas: '.date('Y-m-d H:i'), ['class' => 'footer_clock']);
            ?>
        </div>
    </div>

    <div id="main_menu" class="container">
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-ok-sign"></span>
                <?php echo Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign"></span>
                <?php echo Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>

        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Pagrindinis',
                'url' => Yii::$app->getHomeUrl() . 'index.php',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>
    </div>
</div>

<?php
echo $this->render('_modal');

if (Yii::$app->user->isGuest) {
    echo $this->render('_menu_login');
    echo $this->render('_menu_register');
}

$this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
