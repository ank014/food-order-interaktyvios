<?php

use app\models\User;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$model = new User();
$model->scenario = User::SCENARIO_LOGIN;

Modal::begin([
    'id' => 'login_modal',
    'header' => Html::tag('h4', 'Vartotojo prisijungimas', ['class' => 'modal-title']),
    'footer' => '',
    'size' => Modal::SIZE_LARGE,
]);

$form = ActiveForm::begin([
    'id' => 'login-form',
    'action' => Url::to(['site/user-login']),
]);

echo $form->field($model, 'username', ['options' => ['class' => 'form-group col-xs-6']])->textInput(['class' => 'form-control']);
echo $form->field($model, 'password', ['options' => ['class' => 'form-group col-xs-6']])->passwordInput();
?>

<div class="col-xs-12">
    <p>Norėdami prisijungti, įveskite vartotojo vardą, slaptažodį ir paspauskite „prisijungti".</p>
    <div class="text-center">
        <?= Html::submitButton('Prisijungti', ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
    </div>
</div>

<?php
ActiveForm::end();
Modal::end();