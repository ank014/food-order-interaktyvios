<?php

use app\models\User;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$model = new User();
$model->scenario = User::SCENARIO_CREATE;

Modal::begin([
    'id' => 'register_modal',
    'header' => Html::tag('h4', 'Vartotojo registracija', ['class' => 'modal-title']),
    'footer' => '',
    'size' => Modal::SIZE_LARGE,
 ]);

$form = ActiveForm::begin([
    'id' => 'register-form',
    'action' => Url::to(['site/user-register']),
    'enableAjaxValidation' => true,
    'validationUrl' => Url::to(['site/user-validation']),
]);

echo $form->field($model, 'username', ['options' => ['class' => 'form-group col-xs-6']])->textInput(['class' => 'form-control']);
echo $form->field($model, 'password', ['options' => ['class' => 'form-group col-xs-6']])->passwordInput();
echo $form->field($model, 'email', ['options' => ['class' => 'form-group col-xs-6']])->textInput(['class' => 'form-control']);
echo $form->field($model, 'real_name', ['options' => ['class' => 'form-group col-xs-6']])->textInput(['class' => 'form-control']);
?>

    <div class="col-xs-12">
        <p>Registracijai reikalingi: vartotojas vardas, slaptažodis, el. paštas ir tikrasis vardas.
            Tik tikrasis vardas yra matomas kitiems vartotojams.</p>
        <p>Visi laukai yra privalomi.</p>
        <div class="text-center">
            <?= Html::submitButton('Registruotis', ['class' => 'btn btn-info', 'name' => 'login-button']) ?>
            <?= Html::tag('p', Html::a('Eiti į registracijos puslapį', Url::to(['site/user-register']))) ?>
        </div>
    </div>

<?php
ActiveForm::end();
Modal::end();