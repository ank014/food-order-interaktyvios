<?php

namespace app\models;

/**
 * Global class (global methods used in any file).
 *
 * @author Antanas Šiaulys <antanas.siaulys@ktu.edu>
 *
 * @package app\controllers
 */
class GlobalModel
{
    /**
     * Generates message (growl).
     *
     * @return string
     */
    public static function generateGrowl()
    {
        return "$.growl({
            type: 'growl',
            title: 'Išsaugota',
            message: 'Duomenys sėkmingai išsaugoti',
            duration: 2500,
            location: 'bc',
            size: 'large'
        });";
    }

    /**
     * Returns all 12 months
     *
     * @param null|integer $specific If defined, return specific month only.
     * @return array|string
     */
    public static function getAllMonths($specific = null)
    {
        $months = [
            1 => 'Sausis',
            2 => 'Vasaris',
            3 => 'Kovas',
            4 => 'Balandis',
            5 => 'Gegužė',
            6 => 'Birželis',
            7 => 'Liepa',
            8 => 'Rugpjūtis',
            9 => 'Rugsėjis',
            10 => 'Spalis',
            11 => 'Lapkritis',
            12 => 'Gruodis',
        ];

        if (!is_null($specific)) {
            if ($specific < 1 || $specific > count($months)) {
                return 'Nenustatytas mėnuo';
            } else {
                return $months[$specific];
            }
        } else {
            return $months;
        }
    }
}