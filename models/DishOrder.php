<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "dish_order".
 *
 * @property integer $id
 * @property integer $dish_id
 * @property integer $customer_id
 * @property string $address
 * @property integer $status
 * @property integer $approver_id
 * @property integer $time
 * @property User $approver
 * @property User $customer
 * @property Meal $dish
 */
class DishOrder extends ActiveRecord
{
    /** @const string SCENARIO_EDIT Scenario for editing an order. */
    const SCENARIO_EDIT = 'edit';

    /** @const string SCENARIO_SEARCH Scenario for searching for orders. */
    const SCENARIO_SEARCH = 'search';

    /** @const integer STATUS_PENDING Pending order. */
    const STATUS_PENDING = 1;

    /** @const integer STATUS_APPROVED Approved order. */
    const STATUS_APPROVED = 2;

    /** @const integer STATUS_DECLINED Declined order. */
    const STATUS_DECLINED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dish_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dish_id'], 'required'],
            [['dish_id'], 'integer'],

            [['address'], 'string', 'max' => 255],

            [['status'], 'required'],
            [['status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_EDIT => [
                'address',
                'status',
                'approver_id',
            ],
            self::SCENARIO_SEARCH => [
                'dish_id',
                'customer_id',
                'address',
                'status',
                'approver_id',
                'time',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dish_id' => 'Užsakytas patiekalas',
            'customer_id' => 'Klientas',
            'address' => 'Adresas',
            'status' => 'Būsena',
            'approver_id' => 'Patvirtintojas',
            'time' => 'Laikas',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'approver_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(User::className(), ['id' => 'customer_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDish()
    {
        return $this->hasOne(Meal::className(), ['id' => 'dish_id']);
    }

    /**
     * Searching method.
     *
     * @param array $params Parameters.
     * @return ActiveDataProvider $query
     */
    public function search($params)
    {
        $query = self::find()->joinWith('approver')
            ->joinWith(['customer' => function (ActiveQuery $query) {
                return $query->from(User::tableName() . ' customer');
            }])
            ->joinWith('dish');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere(['=', 'id', $this->id]);
        $query->andFilterWhere(['like', 'dish.name', $this->dish_id]);
        $query->andFilterWhere(['like', 'customer_id', $this->customer_id]);
        $query->andFilterWhere(['like', 'address', $this->address]);
        $query->andFilterWhere(['like', 'approver_id', $this->approver_id]);

        if ($this->status != 0) {
            $query->andFilterWhere(['=', 'status', $this->status]);
        }

        return $dataProvider;
    }

    /**
     * Loads new DishOrder object.
     *
     * @param integer $id Dish ID.
     * @return DishOrder $object Order model.
     * @throws NotFoundHttpException
     */
    public static function loadOrder($id)
    {
        $object = self::findOne($id);

        if (is_null($object)) {
            throw new NotFoundHttpException('Užsakymas nerastas.');
        }

        return $object;
    }

    /**
     * Return a list of orders between 2 timestamps.
     *
     * @param integer $timestampStart Timestamp start.
     * @param integer $timestampEnd Timestamp end.
     * @param null|string $additionalQuery Additional conditional if needed.
     * @return array|ActiveRecord
     */
    public static function getOrdersBetweenMonth($timestampStart, $timestampEnd, $additionalQuery = null)
    {
        $query = self::find()->where('time >= '.$timestampStart.' AND time <= '.$timestampEnd);

        if (!is_null($additionalQuery)) {
            $query->andWhere($additionalQuery);
        }

        return $query->asArray()->all();
    }

    /**
     * Returns the amount of orders taken from specified Meal.
     *
     * @param null|integer $id Meal ID.
     * @return integer
     */
    public static function getAmountFromMealID($id = null)
    {
        $category = Meal::find()->where(['id' => $id])->asArray()->one();

        if (!empty($category)) {
            return count(self::find()->where(['dish_id' => $category['id']])->asArray()->all());
        } else {
            return 0;
        }
    }

    /**
     * Returns order types.
     *
     * @return array
     */
    public static function getOrderTypes()
    {
        $types = [
            0 => 'Visi tipai',
            1 => 'Nepatvirtinta',
            2 => 'Patvirtinta',
            3 => 'Atmesta',
        ];

        return $types;
    }
}
