<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "dish".
 *
 * @property integer $id
 * @property integer $category
 * @property string $name
 * @property string $description
 * @property string $image
 * @property string $price
 * @property integer $is_available
 * @property Category $category0
 * @property DishOrder[] $dishOrders
 */
class Meal extends ActiveRecord
{
    /** @const string SCENARIO_SEARCH Scenario for searching. */
    const SCENARIO_SEARCH = 'Scenario_search';

    /** @const string SCENARIO_NEW Scenario for creating new dish. */
    const SCENARIO_NEW = 'Scenario_new';

    /** @const string SCENARIO_EDIT Scenario for editing information. */
    const SCENARIO_EDIT = 'Scenario_edit';

    /** @const integer MAX_FILE_SIZE Maximum file size (10MB). */
    const MAX_FILE_SIZE = 10485760;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dish';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => '{attribute} negali būti tuščias.'],
            [['name'], 'string', 'length' => [3, 20],
                'tooShort' => '{attribute} yra per trumpas. Turi būti bent jau 3 simboliai.',
                'tooLong' => '{attribute} yra per ilgas. Daugiausiai 20 simbolių.',
            ],

            [['description'], 'required', 'message' => '{attribute} negali būti tuščias.'],
            [['description'], 'string', 'length' => [10, 255],
                'tooShort' => '{attribute} yra per trumpas. Turi būti bent jau 10 simbolių.',
                'tooLong' => '{attribute} yra per ilgas. Daugiausiai 255 simboliai.',
            ],

            [['image'], 'file', 'extensions' => ['png', 'jpg', 'jpeg', 'gif', 'bmp'],
                'wrongExtension' => 'Netinkamas failo formatas. Priimamos galūnės: {extensions}'
            ],
            [['image'], 'file', 'maxSize' => self::MAX_FILE_SIZE,
                'tooBig' => 'Maksimalus leidžiamas dydis: '.(self::MAX_FILE_SIZE / 1048576).' MB',
            ],
            [['image'], 'unique'],

            [['category'], 'required', 'message' => '{attribute} negali būti tuščia.'],
            [['category'], 'integer', 'message' => '{attribute} gali turėti tik skaičius.'],
            [['category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category' => 'id']],

            [['price'], 'required', 'message' => '{attribute} negali būti tuščia.'],
            [['price'], 'double', 'message' => '{attribute} gali turėti tik skaičius.'],

            [['is_available'], 'required', 'message' => '„{attribute}" negali būti tuščias.'],
            [['is_available'], 'integer', 'message' => '{attribute} gali turėti tik skaičius.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_NEW => [
                'category',
                'name',
                'description',
                'image',
                'price',
                'is_available'
            ],
            self::SCENARIO_EDIT => [
                'category',
                'name',
                'description',
                'image',
                'price',
                'is_available'
            ],
            self::SCENARIO_SEARCH => [
                'category',
                'name',
                'description',
                'price',
                'is_available'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Kategorija',
            'name' => 'Pavadinimas',
            'description' => 'Aprašymas',
            'image' => 'Paveikslėlis',
            'price' => 'Kaina',
            'is_available' => 'Leidžiama užsisakyti',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDishOrders()
    {
        return $this->hasMany(DishOrder::className(), ['dish_id' => 'id']);
    }

    /**
     * Loads new Meal object.
     *
     * @param integer $id Dish ID.
     * @return Meal $object Meal model.
     * @throws NotFoundHttpException
     */
    public static function loadMeal($id)
    {
        $object = self::findOne($id);

        if (is_null($object)) {
            throw new NotFoundHttpException('Patiekalas nerastas.');
        }

        return $object;
    }

    /**
     * Searching method.
     *
     * @param array $params Parameters.
     * @return ActiveDataProvider $query
     */
    public function search($params)
    {
        $query = self::find()
            ->select('dish.*, category.id AS cat_id, category.name AS cat_name, category.description AS cat_description')
            ->leftJoin('category', 'dish.category = category.id');

        $dataProvider = new ActiveDataProvider([
           'query' => $query,
        ]);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere(['=', 'id', $this->id]);
        $query->andFilterWhere(['=', 'category', $this->category]);
        $query->andFilterWhere(['like', 'dish.name', $this->name]);
        $query->andFilterWhere(['=', 'description', $this->description]);

        if (!empty($params['Meal']['price'])) {
            $priceMin = (integer) explode(',', $params['Meal']['price'])[0];
            $priceMax = (integer) explode(',', $params['Meal']['price'])[1];
            $query->andFilterWhere(['between', 'price', $priceMin, $priceMax]);
        }

        if ($this->is_available == 1) {
            $query->andFilterWhere(['=', 'is_available', $this->is_available]);
        }

        return $dataProvider;
    }
}
