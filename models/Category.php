<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $description
 * @property Meal $dishes
 */
class Category extends ActiveRecord
{
    /** @const string SCENARIO_NEW Scenario for creating new category. */
    const SCENARIO_NEW = 'menu_new';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer', 'message' => 'Netinkama reikšmė.'],

            [['name'], 'string', 'max' => 30, 'message' => 'Pavadinimą gali sudaryti daugiausiai 30 simbolių.'],

            [['description'], 'string', 'max' => 255, 'message' => 'Aprašymą gali sudaryti daugiausiai 30 simbolių.'],

            [['parent_id', 'name', 'description'], 'required', 'message' => '{attribute} negali būti tuščias.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_NEW => [
                'parent_id',
                'name',
                'description',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Tėvinė kategorija',
            'name' => 'Pavadinimas',
            'description' => 'Aprašymas',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDishes()
    {
        return $this->hasMany(Meal::className(), ['category' => 'id']);
    }

    /**
     * Searching method.
     *
     * @return ActiveDataProvider $query
     */
    public function search()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => self::find()->where(['parent_id' => 0]),
            'sort' => false,
        ]);

        return $dataProvider;
    }

    /**
     * Searching method.
     *
     * @return ActiveDataProvider $query
     */
    public function searchRootOnly()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => self::find()->where('parent_id != 0'),
            'sort' => false,
        ]);

        return $dataProvider;
    }

    /**
     * Loads new DishOrder object.
     *
     * @param integer $id Category ID.
     * @return Category $object Category model.
     * @throws NotFoundHttpException
     */
    public static function loadCategory($id)
    {
        $object = self::findOne($id);

        if (is_null($object)) {
            throw new NotFoundHttpException('Kategorija nerasta.');
        }

        return $object;
    }

    /**
     * Returns root (parent_id = 0) categories.
     *
     * @return array|ActiveRecord
     */
    public static function getRootCategories()
    {
        return self::find()->select('category.id, category.name')->joinWith('dishes')->where(['parent_id' => 0])->asArray()->all();
    }

    /**
     * Returns all categories that are not in root.
     *
     * @return array
     */
    public static function getCategoryListAsDropdown()
    {
        return ArrayHelper::map(self::find()->where('parent_id != 0')->asArray()->all(), 'id', 'name');
    }

    /**
     * Combines root and non-root categories as dropdown.
     *
     * @return array $combined Combined categories.
     */
    public static function getCombinedCategories()
    {
        $query = self::find()->where('parent_id = 0')->asArray()->all();
        $combined = [];

        foreach ($query as $q) {
            $query1 = self::find()->where('parent_id = '.$q['id'])->asArray()->all();
            $combined[$q['name']]  = ArrayHelper::map($query1, 'id', 'name');
        }

        return $combined;
    }

    /**
     * Constructs category chain for Meal as string.
     *
     * @param null|integer $id Meal ID.
     * @return string
     */
    public static function getCategoryChain($id = null)
    {
        $mealInfo = Meal::find()->where(['id' => $id])->asArray()->one();
        $nonRootCategory = self::find()->where(['id' => $mealInfo['category']])->asArray()->one();
        $rootCategory = self::find()->where(['id' => $nonRootCategory['parent_id']])->asArray()->one();

        $chain = '„'.$rootCategory['name'].'" / „'.$nonRootCategory['name'].'"';

        return $chain;
    }
}
