<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;


/**
 * Log class for action logging.
 *
 * @author Antanas Šiaulys <antanas.siaulys@ktu.edu>
 *
 * @package app\controllers
 */
class LogModel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'type', 'content', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Vartotojo vardas',
            'type' => 'Tipas',
            'content' => 'Pranešimas',
            'date' => 'Data'
        ];
    }

    /**
     * Searches for specific rows through $params.
     *
     * @param array $params A list of attributes.
     * @return object $dataProvider DataProvider.
     */
    public function search($params)
    {
        $query = self::find()->select('log.*, user.username')->leftJoin('user', 'log.user_id = user.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(['=', 'log.id', $params['LogModel']['id']]);
        $query->andFilterWhere(['like', 'user.username', $params['LogModel']['user_id']]);
        $query->andFilterWhere(['=', 'log.type', $params['LogModel']['type']]);
        $query->andFilterWhere(['like', 'content', $params['LogModel']['content']]);

        if (!empty($params['LogModel']['date'])) {
            date_default_timezone_set('Europe/Vilnius');
            $query->andFilterWhere(['between', 'date', strtotime($params['LogModel']['date']), strtotime($params['LogModel']['date']) + 86400]);
        }

        return $dataProvider;
    }

    /**
     * Registers log after altering table.
     *
     * @param integer $type Log type.
     * @param string $content Text.
     */
    public static function registerLog($type, $content)
    {
        $self = new self();
        $self->user_id = Yii::$app->user->identity->attributes['id'];
        $self->type = $type;
        $self->content = $content;
        $self->date = time();
        $self->save();
    }
}