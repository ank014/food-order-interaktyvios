<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User class (login, register, logout, identity).
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $real_name
 * @property string $email
 * @property string $auth_key
 * @property integer $type
 *
 * @author Antanas Šiaulys <antanas.siaulys@ktu.edu>
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{
    /** @const SCENARIO_CREATE Scenario for creating new User. */
    const SCENARIO_CREATE = 'creation';

    /** @const SCENARIO_ADMIN_CREATE Scenario for creating through administration. */
    const SCENARIO_ADMIN_CREATE = 'admin_creation';

    /** @const SCENARIO_EDIT Scenario for editing user information. */
    const SCENARIO_EDIT = 'edition';

    /** @const SCENARIO_LOGIN Scenario for logging in. */
    const SCENARIO_LOGIN = 'login';

    /** @const SCENARIO_SEARCH Scenario for searching. */
    const SCENARIO_SEARCH = 'search';

    /** @const USER_TYPE_0 All types */
    const USER_TYPE_0 = 0;

    /** @const USER_TYPE_1 Member */
    const USER_TYPE_1 = 1;

    /** @const USER_TYPE_2 Staff */
    const USER_TYPE_2 = 2;

    /** @const USER_TYPE_3 Administrator */
    const USER_TYPE_3 = 3;

    /** @var $_user User object */
    public $_user;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'username',
                'real_name',
                'password',
                'email',
            ],
            self::SCENARIO_ADMIN_CREATE => [
                'username',
                'real_name',
                'password',
                'email',
                'type',
            ],
            self::SCENARIO_LOGIN => [
                'username',
                'password',
            ],
            self::SCENARIO_EDIT => [
                'username',
                'password',
                'email',
                'type',
            ],
            self::SCENARIO_SEARCH => [
                'id',
                'username',
                'real_name',
                'email',
                'type',
            ]
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username'], 'required', 'message' => '{attribute} negali būti tuščias.'],
            [['username'], 'unique', 'except' => self::SCENARIO_LOGIN, 'message' => '{attribute} turi būti unikalus.'],
            [['username'], 'string', 'length' => [3, 20],
                'tooShort' => '{attribute} yra per trumpas. Turi būti bent jau 3 simboliai.',
                'tooLong' => '{attribute} yra per ilgas. Daugiausiai 20 simbolių.'
            ],

            [['real_name'], 'required', 'message' => '{attribute} negali būti tuščias.'],
            [['real_name'], 'string', 'length' => [3, 20],
                'tooShort' => '{attribute} yra per trumpas. Turi būti bent jau 3 simboliai.',
                'tooLong' => '{attribute} yra per ilgas. Daugiausiai 20 simbolių.'
            ],

            [['password'], 'required', 'message' => '{attribute} negali būti tuščias.'],
            [['password'], 'validatePassword', 'on' => self::SCENARIO_LOGIN, 'message' => 'Neteisingas {attribute}.'],
            [['password'], 'string', 'length' => [5, 80],
                'tooShort' => '{attribute} yra per trumpas. Turi būti bent jau 5 simboliai.',
                'tooLong' => '{attribute} yra per ilgas. Daugiausiai 80 simbolių.'
            ],

            [['email'], 'email', 'message' => '{attribute} turi būti el. pašto formato.'],
            [['email'], 'required', 'on' => self::SCENARIO_CREATE, 'message' => '{attribute} negali būti tuščias.'],
            [['email'], 'string', 'length' => [5, 40],
                'tooShort' => '{attribute} yra per trumpas. Turi būti bent jau 5 simboliai.',
                'tooLong' => '{attribute} yra per ilgas. Daugiausiai 40 simbolių.'
            ],

            [['type'], 'integer'],
            [['type'], 'in', 'range' => [1, 2, 3]],
        ];
    }

    /**
     * Validates the password. This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePasswordHash($this->password))
                $this->addError($attribute, 'Neteisingas vartotojo vardas arba slaptažodis.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Vartotojo vardas',
            'real_name' => 'Tikrasis vardas',
            'password' => 'Slaptažodis',
            'email' => 'El. paštas',
            'type' => 'Tipas',
        ];
    }

    /**
     * Returns a list of possible User types.
     *
     * @param boolean $includePlaceholder
     * @return array A list of possible types.
     */
    public static function getTypeName($includePlaceholder = false)
    {
        if ($includePlaceholder) {
            return [
                self::USER_TYPE_0 => 'Visi tipai',
                self::USER_TYPE_1 => 'Narys',
                self::USER_TYPE_2 => 'Padavėjas',
                self::USER_TYPE_3 => 'Administratorius',
            ];
        } else {
            return [
                self::USER_TYPE_0 => 'Mobilios programėlės naudotojas',
                self::USER_TYPE_1 => 'Narys',
                self::USER_TYPE_2 => 'Padavėjas',
                self::USER_TYPE_3 => 'Administratorius',
            ];
        }
    }

    /**
     * Searches for specific rows through $params.
     *
     * @param array $params A list of attributes.
     * @return object $dataProvider DataProvider.
     */
    public function search($params)
    {
        $query = self::find()->select(['*']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(['=', 'id', $this->id]);
        $query->andFilterWhere(['like', 'real_name', $this->real_name]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        if ($this->type != 0) {
            $query->andFilterWhere(['=', 'type', $this->type]);
        }

        return $dataProvider;
    }

    /**
     * Retrieves user's name by user's ID.
     *
     * @param integer $id User ID.
     * @return array|string Username (and with/without ID).
     */
    public static function getUserName($id)
    {
        $query = self::find()->select(['id', 'username'])->where(['id' => $id])->asArray()->all();

        return (!empty($query[0]['username'])) ? $query[0]['username'] : '<i>Vartotojas pašalintas</i>';
    }

    /**
     * Gets User details.
     *
     * @param integer $id User ID.
     * @return array|ActiveRecord
     */
    public static function getUserDetails($id)
    {
        return self::find()->select('*')->where(['id' => $id])->asArray()->one();
    }

    /**
     * Creates a new user. Default type: 1.
     *
     * @param array $attributes
     * @return boolean
     */
    public function createNewUser($attributes)
    {
        if (is_null($attributes['User']['password']))
            return false;

        $this->load($attributes);
        $this->password = Yii::$app->security->generatePasswordHash($attributes['User']['password']);
        $this->auth_key = Yii::$app->security->generateRandomString();

        if ($this->scenario == self::SCENARIO_CREATE) {
            $this->type = 1;
        }

        return $this->save();
    }

    /**
     * Returns all Users for dropdown.
     *
     * @param boolean $includePlaceholder Include a placeholder?
     * @return array $users A list of all Users.
     */
    public static function getAllUsersAsDropdown($includePlaceholder = false)
    {
        $query = self::find()->asArray()->all();

        if ($includePlaceholder) {
            array_unshift($query, ['real_name' => 'Visi vartotojai']);
        }

        return ArrayHelper::map($query, 'id', 'real_name');
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean Whether the user is logged in successfully.
     */
    public function login()
    {
        if ($this->validate())
            return Yii::$app->user->login($this->getUser(), 3600 * 24 * 30);

        return false;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_key)
    {
        return $this->auth_key === $auth_key;
    }

    /**
     * @inheritdoc
     */
    private function validatePasswordHash($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Finds user by [username].
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null)
            $this->_user = self::findByUsername($this->username);

        return $this->_user;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds user by username.
     *
     * @param string $username.
     *
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
}
