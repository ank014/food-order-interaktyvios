<?php

namespace app\controllers;

use app\models\DishOrder;
use app\models\GlobalModel;
use app\models\User;
use Yii;
use yii\base\InvalidCallException;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * SiteController controller class.
 *
 * @author Antanas Šiaulys <antanas.siaulys@ktu.edu>
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'user-login', 'user-register', 'user-validation'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'error', 'index',
                            'user-admin-register', 'user-logout', 'user-list', 'user-view', 'change-user', 'user-remove', 'user-validation',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $months = '';
        $ordersAmount = '';
        $specificUserApproved = '';

        for ($i = date('n') + 1; $i <= 12; $i++) {
            $time[0] = gmmktime(0, 0, 0, $i, 1, date('Y') - 1);
            $time[1] = gmmktime(23, 59, 59, $i, cal_days_in_month(CAL_GREGORIAN, $i, date('Y')), date('Y') - 1);
            $ordersAmount .= count(DishOrder::getOrdersBetweenMonth($time[0], $time[1])).', ';
            $specificUserApproved .= count(DishOrder::getOrdersBetweenMonth(
                    $time[0], $time[1], 'approver_id = '.Yii::$app->user->id
                )).', ';
            $months .= '"'.GlobalModel::getAllMonths($i).'", ';
        }

        for ($i = date('n'); $i > 0; $i--) {
            $time[0] = gmmktime(0, 0, 0, $i, 1, date('Y'));
            $time[1] = gmmktime(23, 59, 59, $i, cal_days_in_month(CAL_GREGORIAN, $i, date('Y')), date('Y'));
//          $ordersAmount[$j] = count(DishOrder::getOrdersBetweenMonth($time[0], $time[1])); // Fits if array
            $ordersAmount .= count(DishOrder::getOrdersBetweenMonth($time[0], $time[1])).', ';
            $specificUserApproved .= count(DishOrder::getOrdersBetweenMonth(
                    $time[0], $time[1], 'approver_id = '.Yii::$app->user->id
                )).', ';
            $months .= '"'.GlobalModel::getAllMonths($i).'", ';
        }

        $months = substr($months, 0, -2);
        $ordersAmount = substr($ordersAmount, 0, -2);
        $specificUserApproved = substr($specificUserApproved, 0, -2);

        return $this->render('index', [
            'list' => $months,
            'ordersAmount' => $ordersAmount,
            'specificUserApproved' => $specificUserApproved,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionUserLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $post = Yii::$app->request->post();
        $model = new User();
        $model->scenario = User::SCENARIO_LOGIN;

        if ($model->load($post) && $model->login()) {
            Yii::$app->session->setFlash('success', 'Sėkmingai prisijungėte prie sąskaitos.');

            return $this->redirect(['index']);
        } elseif (Yii::$app->request->isPost) {
            Yii::$app->session->setFlash('error', 'Prisijungimas nepavyko. Blogi prisijungimo duomenys.');

            return $this->redirect(['index']);
        }

        return $this->render('user-login', [
            'model' => $model,
        ]);
    }

    /**
     * Register action.
     *
     * @return string
     */
    public function actionUserRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $post = Yii::$app->request->post();
        $model = new User();
        $model->scenario = User::SCENARIO_CREATE;

        if (!empty($post)) {
            if ($model->createNewUser($post)) {
                Yii::$app->session->setFlash('success', 'Vartotojo sąskaita sėkmingai sukurta.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Vartotojo sąskaitos sukurti nepavyko. Pabandykite dar kartą.');
            }
        }

        return $this->render('user-register', [
            'model' => $model,
        ]);
    }

    /**
     * Register action for first user.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionUserAdminRegister()
    {
        if (Yii::$app->user->identity->type < 3) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $post = Yii::$app->request->post();
        $model = new User();
        $model->scenario = User::SCENARIO_CREATE;

        if (!empty($post)) {
            if ($model->createNewUser($post)) {
                Yii::$app->session->setFlash('success', 'Vartotojo sąskaita sėkmingai sukurta.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Vartotojo sąskaitos sukurti nepavyko. Pabandykite dar kartą.');
            }
        }

        return $this->render('user-admin-register', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionUserLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays all users.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionUserList()
    {
        if (Yii::$app->user->identity->type < 3) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $user = new User();
        $user->scenario = User::SCENARIO_SEARCH;
        $userProvider = $user->search(Yii::$app->request->get());

        return $this->render('user-list', [
            'model' => $user,
            'modelProvider' => $userProvider,
        ]);
    }

    /**
     * Renders profile viewing.
     *
     * @param integer $id User's ID.
     * @return string
     * @throws HttpException
     */
    public function actionUserView($id = null)
    {
        if (Yii::$app->user->identity->type < 3) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (empty($id)) {
            throw new ForbiddenHttpException('Trūksta parametrų.');
        }

        if ($id == 1 && Yii::$app->user->id != $id) {
            throw new ForbiddenHttpException('Pagrindinio administratoriaus sąskaita negali būti peržiūrėta arba koreguota.');
        }

        $model = (new User())->findOne(['id' => $id]);

        if (is_null($model)) {
            throw new NotFoundHttpException('Vartotojas nerastas.');
        }

        return $this->render('user-view', [
            'model' => $model,
        ]);
    }

    /**
     * Changes information about User.
     *
     * @param integer $id User ID.
     * @return string json_encode Results.
     * @throws HttpException User not found.
     */
    public function actionChangeUser($id = null)
    {
        if (Yii::$app->user->identity->type < 3) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $post = Yii::$app->request->post();

        if (empty($post)) {
            throw new ForbiddenHttpException('Tiesioginis priėjimas draudžiamas.');
        }

        $attribute = key($post['User']);

        if (!in_array($attribute, ['username', 'real_name', 'password', 'email', 'type'])) {
            return json_encode(['output' => '', 'message' => 'Redaguojamas atributas nepriimamas.']);
        } elseif (Yii::$app->user->identity->type < 3) {
            return json_encode(['output' => '', 'message' => 'Tik administratorius gali vykdyti šiuos veiksmus.']);
        } elseif ($id == 1) {
            return json_encode(['output' => '', 'message' => 'Pagr. administratoriaus duomenų keisti negalima.']);
        }

        $model = (new User())->findOne($id);

        if (is_null($model)) {
            return json_encode(['output' => '', 'message' => 'Vartotojas nerastas.']);
        }

        $model->scenario = User::SCENARIO_EDIT;
        $model->$attribute = $post['User'][$attribute];

        if ($attribute == 'type') {
            if (!in_array($post['User']['type'], [1, 2, 3])) {
                return json_encode(['output' => '', 'message' => 'Netinkamas tipas.']);
            }

            $post['User'][$attribute] = User::getTypeName()[$post['User']['type']];
        }

        if ($model->save())
            return json_encode(['output' => Html::encode($post['User'][$attribute]), 'message' => '']);
        else
            return json_encode(['output' => '', 'message' => $model->firstErrors[$attribute]]);
    }

    /**
     * Removes existing user.
     *
     * @param integer $id User's ID.
     * @return string
     * @throws HttpException
     */
    public function actionUserRemove($id = null)
    {
        if (Yii::$app->user->identity->type < 3) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (empty($id)) {
            throw new ForbiddenHttpException('Trūksta parametrų.');
        }

        if ($id == 1) {
            throw new ForbiddenHttpException('Pagrindinio administratoriaus ištrinti negalima.');
        }

        $user = User::findOne($id);

        if (!is_null(DishOrder::find()->where('customer_id = '.$id.' OR approver_id = '.$id)->asArray()->one())) {
            Yii::$app->session->setFlash('error', 'Vartotojo pašalinti negalima, nes jis yra arba pateikęs, arba patvirtinęs bent vieną užsakymą.');
        } elseif (is_null($user)) {
            Yii::$app->session->setFlash('error', 'Vartotojas nerastas.');
        } else {
            if ($user->delete()) {
                Yii::$app->session->setFlash('success', 'Vartotojas buvo sėkmingai ištrintas.');
            } else {
                Yii::$app->session->setFlash('error', 'Vartotojo ištrinti nepavyko.');
            }
        }

        return $this->redirect(['site/user-list']);
    }

    /**
     * Validates form.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionUserValidation()
    {
        if (!Yii::$app->request->isPost) {
            throw new InvalidCallException('Tiesioginis priėjimas uždraustas.');
        }

        $model = new User();
        $model->scenario = User::SCENARIO_CREATE;
        $model->load(Yii::$app->request->post());

        return json_encode(ActiveForm::validate($model));
    }
}
