<?php

namespace app\controllers;

use app\models\Category;
use app\models\DishOrder;
use app\models\Meal;
use Yii;
use yii\base\InvalidCallException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * MealController controller class.
 *
 * @author Antanas Šiaulys <antanas.siaulys@ktu.edu>
 * @package app\controllers
 */
class MealController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'meal-index', 'meal-view', 'meal-add', 'meal-change-status', 'meal-remove', 'meal-validation',
                            'order-index', 'order-approve', 'order-decline',
                            'menu-index', 'menu-add', 'menu-view', 'menu-remove', 'menu-validation',
                            'error',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Renders and displays meals list.
     *
     * @return string
     */
    public function actionMealIndex()
    {
        $model = new Meal();
        $model->scenario = Meal::SCENARIO_SEARCH;
        $rootCategories = ArrayHelper::map(Category::getRootCategories(), 'id', 'name');

        $priceMin = 0;
        $priceMax = 0;

        $completeList = $model->search([])->getModels();
        foreach ($completeList as $row) {
            if ($row->price < $priceMin || $priceMin == 0) {
                $priceMin = round($row->price) - 1;
            } elseif ($row->price > $priceMax || $priceMax == 0) {
                $priceMax = round($row->price) + 1;
            }
        }

        return $this->render('meal-index', [
            'dataProvider' => $model->search(Yii::$app->request->get()),
            'model' => $model,
            'categoriesList' => $rootCategories,
            'priceMin' => $priceMin,
            'priceMax' => $priceMax,
        ]);
    }

    /**
     * Renders and updates meal information.
     *
     * @param null|integer $id Patiekalo ID.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMealView($id = null)
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (is_null($id)) {
            throw new InvalidParamException('Trūksta parametrų.');
        }

        $post = Yii::$app->request->post();
        $model = Meal::loadMeal($id);

        if (empty($post)) {
            return $this->render('meal-add', [
                'model' => $model,
            ]);
        }

        $model->scenario = Meal::SCENARIO_EDIT;
        $image = $model->image;
        $model->load($post);
        $model->image = (!empty($post['removeOldImage']) ? '' : $image);

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Patiekalo duomenys sėkmingai atnaujinti.');
        } else {
            Yii::$app->session->setFlash('error', 'Patiekalo duomenų nepavyko atnaujinti. Galimai įvesti klaidingi duomenys.');
        }

        return $this->redirect(['meal/meal-index']);
    }

    /**
     * Renders and/or adds new Meal.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMealAdd()
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $post = Yii::$app->request->post();
        $model = new Meal();
        $model->scenario = Meal::SCENARIO_NEW;

        if (empty($post)) {
            return $this->render('meal-add', [
                'model' => $model,
            ]);
        }

        $folder = Yii::$app->basePath.'/uploads';

        if (!file_exists($folder)) {
            mkdir($folder);
        }

        $model->load($post);

        if ($model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');

            if (!is_null($model->image)) {
                if (!is_null(Meal::findOne(['image' => $model->image->baseName . '.' . $model->image->extension]))) {
                    Yii::$app->session->setFlash('error', 'Toks failas jau egzistuoja. Parinkite failui naują pavadinimą.');

                    return $this->redirect(['meal/meal-add']);
                }

                $model->image->saveAs($folder.'/'.$model->image->baseName.'.'.$model->image->extension);
                $model->image = $model->image->baseName . '.' . $model->image->extension;
            }

            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Naujas patiekalas sėkmingai įkeltas.');
            } else {
                Yii::$app->session->setFlash('error', 'Naujo patiekalo įkelti nepavyko. Pabandykite dar kartą.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Validacija nesėkminga (buvo įvesta klaidingų duomenų).');
        }

        return $this->redirect(['meal/meal-index']);
    }

    /**
     * Changes status to opposite.
     *
     * @param null|integer $id Meal ID.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMealChangeStatus($id = null)
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (is_null($id)) {
            throw new InvalidParamException('Trūksta parametrų.');
        }

        $model = Meal::loadMeal($id);
        $model->scenario = Meal::SCENARIO_EDIT;
        $model->is_available = ($model->is_available ? 0 : 1);

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Patiekalo būsena sėkmingai pakeista.');
        } else {
            Yii::$app->session->setFlash('error', 'Patiekalo būsenos pakeisti nepavyko. Pabandykite dar kartą.');
        }

        return $this->redirect(['meal/meal-index']);
    }

    /**
     * Removes existing meal.
     *
     * @param null|integer $id Meal ID.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMealRemove($id = null)
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (is_null($id)) {
            throw new InvalidParamException('Trūksta parametrų.');
        }

        $model = Meal::loadMeal($id);

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Patiekalas sėkmingai ištrintas.');
        } else {
            Yii::$app->session->setFlash('error', 'Patiekalo ištrinti nepavyko. Pabandykite dar kartą.');
        }

        return $this->redirect(['meal/meal-index']);
    }

    /**
     * Validates Meal form.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMealValidation()
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (!Yii::$app->request->isPost) {
            throw new InvalidCallException('Tiesioginis priėjimas uždraustas.');
        }

        $model = new Meal();
        $model->scenario = Meal::SCENARIO_NEW;
        $model->load(Yii::$app->request->post());

        return json_encode(ActiveForm::validate($model));
    }

    /**
     * Renders View file for orders list.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionOrderIndex()
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $model = new DishOrder();
        $model->scenario = DishOrder::SCENARIO_SEARCH;

        return $this->render('order-index', [
            'dataProvider' => $model->search(Yii::$app->request->get()),
            'model' => $model,
        ]);
    }

    /**
     * Marks Order as approved.
     *
     * @param null|integer $id Order ID.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionOrderApprove($id = null)
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (is_null($id)) {
            throw new InvalidParamException('Trūksta parametrų.');
        }

        $model = DishOrder::loadOrder($id);

        if ($model->status != 1) {
            throw new InvalidParamException('Pakeisti būsenos negalima.');
        }

        $model->scenario = DishOrder::SCENARIO_EDIT;
        $model->approver_id = Yii::$app->user->identity->id;
        $model->status = 2;

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Užsakymas patvirtintas.');
        } else {
            Yii::$app->session->setFlash('error', 'Užsakymas patvirtinti nepavyko. Pabandykite dar kartą.');
        }

        return $this->redirect(['meal/order-index']);
    }

    /**
     * Marks Order as approved.
     *
     * @param null|integer $id Order ID.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionOrderDecline($id = null)
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (is_null($id)) {
            throw new InvalidParamException('Trūksta parametrų.');
        }

        $model = DishOrder::loadOrder($id);

        if ($model->status != 1) {
            throw new InvalidParamException('Pakeisti būsenos negalima.');
        }

        $model->scenario = DishOrder::SCENARIO_EDIT;
        $model->approver_id = Yii::$app->user->identity->id;
        $model->status = 3;

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Užsakymas patvirtintas.');
        } else {
            Yii::$app->session->setFlash('error', 'Užsakymas patvirtinti nepavyko. Pabandykite dar kartą.');
        }

        return $this->redirect(['meal/order-index']);
    }

    /**
     * Renders menu list.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMenuIndex()
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $model = new Category();

        return $this->render('menu-index', [
            'model' => $model,
            'dataProvider' => $model->search(),
            'dataProviderRootOnly' => $model->searchRootOnly(),
        ]);
    }

    /**
     * Renders and adds new Category.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMenuAdd()
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $post = Yii::$app->request->post();
        $model = new Category();
        $model->scenario = Category::SCENARIO_NEW;

        if (empty($post)) {
            return $this->render('menu-add', [
                'model' => $model,
            ]);
        }

        $model->load($post);

        if (empty(Category::findOne($model->parent_id))) {
            throw new InvalidParamException('Pasirinkta kategorija nerasta.');
        }

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Kategorija sėkmingai sukurta.');
        } else {
            Yii::$app->session->setFlash('error', 'Kategorijos sukurti nepavyko.');
        }

        return $this->redirect(['meal/menu-index']);
    }

    /**
     * Renders View page.
     *
     * @param null|integer $id Category ID.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMenuView($id = null)
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        $post = Yii::$app->request->post();
        $model = Category::loadCategory($id);
        $model->scenario = Category::SCENARIO_NEW;

        if (empty($post)) {
            return $this->render('menu-add', [
                'model' => $model
            ]);
        }

        $model->load($post);

        if (empty(Category::findOne($model->parent_id))) {
            throw new InvalidParamException('Pasirinkta kategorija nerasta.');
        }

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Kategorijos duomenys atnaujinti.');
        } else {
            Yii::$app->session->setFlash('error', 'Kategorijos duomenų atnaujinti nepavyko.');
        }

        return $this->redirect(['meal/menu-index']);
    }

    /**
     * Removes Category.
     *
     * @param null|integer $id Category ID.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMenuRemove($id = null)
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (is_null($id)) {
            throw new InvalidParamException('Trūksta parametrų.');
        }

        $model = Category::loadCategory($id);

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Pasirinkta kategorija pašalinta.');
        } else {
            Yii::$app->session->setFlash('error', 'Pašalinti pasirinktos kategorijos nepavyko. Pabandykite dar kartą.');
        }

        return $this->redirect(['meal/menu-index']);
    }

    /**
     * Validates Category form.
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionMenuValidation()
    {
        if (Yii::$app->user->identity->type < 2) {
            throw new ForbiddenHttpException('Leidimas neduotas.');
        }

        if (!Yii::$app->request->isPost) {
            throw new InvalidCallException('Tiesioginis priėjimas uždraustas.');
        }

        $model = new Category();
        $model->scenario = Category::SCENARIO_NEW;
        $model->load(Yii::$app->request->post());

        return json_encode(ActiveForm::validate($model));
    }

}