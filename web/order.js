$('.confirmationButton').on('click', function(e) {
    e.preventDefault();
    $('#confirm_modal_button').attr('href', targetURL + $(this).parent('a').data('url') + '&id=' + $(this).parent('a').data('id'));
    $('#confirm_modal').modal('show');
});