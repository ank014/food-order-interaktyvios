$(document).ready(function() {
    $('[data-toggle=tooltip]').tooltip();
    $('[data-toggle=popover]').popover();

    startTime();

    $('#menu_login').on('click', function() {
        $('#login_modal').modal('show');
    });

    $('#menu_register').on('click', function() {
        $('#register_modal').modal('show');
    });

    $('.removalButton').on('click', function(e) {
        e.preventDefault();
        $('#confirm_modal_button').attr('href', targetURL + '&id=' + $(this).parent('a').data('id'));
        $('#confirm_modal').modal('show');
    });
});

function startTime()
{
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('.footer_clock')[0].innerHTML = 'Serverio laikas: ' + h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}

function checkTime(i)
{
    if (i < 10) {
        i = "0" + i;
    }

    return i;
}