-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2017 at 09:50 AM
-- Server version: 5.6.31
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) NOT NULL,
  `parent_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `description`) VALUES
(1, 0, 'Karštieji patiekalai', 'Karštieji patiekalai'),
(2, 0, 'Jūrų gėrybės', 'Jūrų gėrybės'),
(3, 1, 'Sriubos', 'Sriubos'),
(4, 1, 'Kepsniai', 'Įvairūs kepsniai');

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE `dish` (
  `id` int(10) NOT NULL,
  `category` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(510) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `is_available` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dish`
--

INSERT INTO `dish` (`id`, `category`, `name`, `description`, `image`, `price`, `is_available`) VALUES
(1, 1, 'Vištienos sriuba', 'Vištienos sriuba su vištiena.', '54f4a5bf1042a_-_chicken-noodle-soup-recipe.jpg', '2.50', 1),
(2, 1, 'Žirnių sriuba', 'Žirnių sriuba su žirniais ir kitomis daržovėmis.', 'zirniu-sriuba-su-imbieru-1024x680.jpg', '3.00', 1),
(3, 1, 'Pipirų sriuba', 'Su pipirais.', 'Pepper_Soup_1524.jpg', '2.80', 0),
(4, 2, 'Lydeka', 'Tiesiog lydeka', 'lydeka2.jpg', '4.50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dish_order`
--

CREATE TABLE `dish_order` (
  `id` int(10) NOT NULL,
  `dish_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `approver_id` int(10) DEFAULT NULL,
  `time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dish_order`
--

INSERT INTO `dish_order` (`id`, `dish_id`, `customer_id`, `address`, `status`, `approver_id`, `time`) VALUES
(3, 1, 2, 'Adresiukas 45', 2, 2, 1455459890),
(4, 2, 3, 'Šiaulių g. 7', 2, 2, 1455959820),
(5, 3, 5, 'Panevėžio g. 4', 2, 2, 1455952870),
(6, 4, 5, 'Stulpių g. 79', 2, 3, 1455959670),
(7, 2, 2, 'Not Found :|', 2, 2, 1455959840),
(8, 4, 3, 'Planeta Žemė', 2, 2, 1455959800),
(9, 1, 2, 'Adresiukas 45', 2, 2, 1456852270),
(10, 2, 3, 'Šiaulių g. 7', 2, 1, 1456846270),
(11, 3, 5, 'Panevėžio g. 4', 2, 2, 1456842870),
(12, 4, 5, 'Stulpių g. 79', 2, 2, 1456842290),
(13, 2, 2, 'Not Found :|', 1, NULL, 1456842355),
(14, 1, 2, 'Adresiukas 45', 2, 1, 1459570670),
(15, 2, 3, 'Šiaulių g. 7', 2, 1, 1459520870),
(16, 3, 5, 'Panevėžio g. 4', 2, 3, 1459620670),
(17, 1, 2, 'Adresiukas 45', 2, 1, 1462162670),
(18, 2, 3, 'Šiaulių g. 7', 2, 1, 1462115670),
(19, 3, 5, 'Panevėžio g. 4', 2, 2, 1462112870),
(20, 4, 5, 'Stulpių g. 79', 2, 1, 1462112690),
(21, 2, 2, 'Not Found :|', 2, 2, 1462112630),
(22, 4, 3, 'Planeta Žemė', 2, 3, 1462112695),
(23, 1, 2, 'Adresiukas 45', 2, 1, 1462162670),
(24, 2, 3, 'Šiaulių g. 7', 2, 3, 1462125670),
(25, 3, 5, 'Panevėžio g. 4', 2, 1, 1463112870),
(26, 4, 5, 'Stulpių g. 79', 2, 2, 1462152690),
(27, 2, 2, 'Not Found :|', 2, 3, 1462122630),
(28, 4, 3, 'Planeta Žemė', 1, NULL, 1462142695),
(29, 1, 2, 'Adresiukas 45', 2, 2, 1465791070),
(30, 2, 3, 'Šiaulių g. 7', 2, 3, 1464891070),
(31, 3, 5, 'Panevėžio g. 4', 2, 1, 1465791270),
(32, 4, 5, 'Stulpių g. 79', 2, 2, 1464799070),
(33, 2, 2, 'Not Found :|', 2, 1, 1464791080),
(34, 4, 3, 'Planeta Žemė', 2, 2, 1464791550),
(35, 1, 2, 'Adresiukas 45', 2, 3, 1464791055),
(36, 2, 3, 'Šiaulių g. 7', 2, 3, 1464791045),
(37, 3, 5, 'Panevėžio g. 4', 2, 3, 1464793370),
(38, 1, 2, 'Adresiukas 45', 2, 3, 1467383070),
(39, 2, 3, 'Šiaulių g. 7', 2, 1, 1467383075),
(40, 3, 5, 'Panevėžio g. 4', 2, 1, 1467363070),
(41, 4, 5, 'Stulpių g. 79', 2, 3, 1467386670),
(42, 2, 2, 'Not Found :|', 2, 3, 1467663070),
(43, 4, 3, 'Planeta Žemė', 2, 2, 1467393070),
(44, 1, 2, 'Adresiukas 45', 2, 1, 1467389970),
(45, 2, 3, 'Šiaulių g. 7', 1, NULL, 1467385470),
(46, 3, 5, 'Panevėžio g. 4', 2, 1, 1467385070),
(47, 4, 5, 'Stulpių g. 79', 2, 2, 1467383700),
(48, 2, 2, 'Not Found :|', 2, 1, 1467383555),
(49, 4, 3, 'Planeta Žemė', 2, 2, 1467383055),
(50, 1, 2, 'Adresiukas 45', 2, 1, 1467383075),
(51, 2, 3, 'Šiaulių g. 7', 2, 2, 1467383170),
(52, 3, 5, 'Panevėžio g. 4', 2, 1, 1467381070),
(53, 4, 5, 'Stulpių g. 79', 2, 3, 1467383270),
(54, 2, 2, 'Not Found :|', 2, 2, 1467383580),
(55, 1, 2, 'Adresiukas 45', 2, 2, 1472739890),
(56, 2, 3, 'Šiaulių g. 7', 2, 3, 1472740870),
(57, 3, 5, 'Panevėžio g. 4', 2, 2, 1472742870),
(58, 4, 5, 'Stulpių g. 79', 2, 3, 1472743870),
(59, 2, 2, 'Not Found :|', 2, 2, 147274330),
(60, 4, 3, 'Planeta Žemė', 2, 1, 1472750870),
(61, 1, 2, 'Adresiukas 45', 2, 1, 1472839870),
(62, 2, 3, 'Šiaulių g. 7', 2, 1, 1472739555),
(63, 3, 5, 'Panevėžio g. 4', 2, 2, 1472739895),
(64, 4, 5, 'Stulpių g. 79', 2, 1, 1472739905),
(65, 2, 2, 'Not Found :|', 2, 1, 1467383595),
(66, 1, 2, 'Adresiukas 45', 1, NULL, 1475531870),
(67, 2, 3, 'Šiaulių g. 7', 2, 2, 1475361870),
(68, 3, 5, 'Panevėžio g. 4', 2, 2, 1476831870),
(69, 4, 5, 'Stulpių g. 79', 2, 1, 1475961870),
(70, 1, 2, 'Adresiukas 45', 2, 3, 1478110270),
(71, 2, 3, 'Šiaulių g. 7', 2, 3, 1478210270),
(72, 3, 5, 'Panevėžio g. 4', 2, 1, 1478310270),
(73, 4, 5, 'Stulpių g. 79', 2, 3, 1478410270),
(74, 2, 2, 'Not Found :|', 2, 3, 1478055270),
(75, 4, 3, 'Planeta Žemė', 2, 2, 1478024270),
(76, 1, 2, 'Adresiukas 45', 1, NULL, 1478010880),
(77, 2, 3, 'Šiaulių g. 7', 2, 2, 1478013870),
(78, 3, 5, 'Panevėžio g. 4', 2, 3, 1478260270),
(79, 4, 5, 'Stulpių g. 79', 2, 1, 1478025270),
(80, 2, 2, 'Not Found :|', 2, 3, 1478256270),
(81, 4, 3, 'Planeta Žemė', 2, 3, 1478010530),
(82, 1, 2, 'Adresiukas 45', 2, 3, 1480652270),
(83, 2, 3, 'Šiaulių g. 7', 2, 3, 1480645270),
(84, 3, 5, 'Panevėžio g. 4', 2, 2, 1480702270),
(85, 4, 5, 'Stulpių g. 79', 2, 2, 1481052270),
(86, 2, 2, 'Not Found :|', 2, 1, 1481202270),
(87, 4, 3, 'Planeta Žemė', 2, 3, 1481502270),
(88, 1, 2, 'Adresiukas 45', 2, 1, 1480606670),
(89, 2, 3, 'Šiaulių g. 7', 2, 3, 1480602555),
(90, 3, 5, 'Panevėžio g. 4', 2, 2, 1480602325),
(91, 4, 5, 'Stulpių g. 79', 2, 2, 1480602935),
(92, 2, 2, 'Not Found :|', 2, 1, 1480602275),
(93, 4, 3, 'Planeta Žemė', 2, 1, 1480633270),
(94, 1, 2, 'Adresiukas 45', 2, 1, 1480699270),
(95, 2, 3, 'Šiaulių g. 7', 2, 3, 1480609070),
(96, 3, 5, 'Panevėžio g. 4', 2, 2, 1480613270),
(97, 1, 2, 'Adresiukas 45', 2, 2, 1483580670),
(98, 2, 3, 'Šiaulių g. 7', 2, 3, 1483290670),
(99, 3, 5, 'Panevėžio g. 4', 2, 1, 1483690670);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `real_name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `type` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `real_name`, `password`, `email`, `auth_key`, `type`) VALUES
(1, 'gytis', 'Gytis', '$2y$13$WySGBm9Bg6lNAyrkqFAjh.rC0ftmMl8ML1IQU20Tf1sgtR9zPc2BW', 'axortt@gmail.com', 'Fh44b35TmmtNEyAOyswTIbz7mP06BnrI', 3),
(2, 'admin', 'Admin', '$2y$13$d2gaHsAUO30VqfytRQzrKewl/Oxi.2sbBlJYe0pgEs5wBrSs6TOD2', 'admin@gmail.com', 'BS5C_-Th26wBRbMRHWJRjDyd4D-Q7Uvt', 3),
(3, 'chef', 'Chef', '$2y$13$S5xIIgCkGeASiH7cgA8Fy.oGenxGbtoLfmTkVBmssROrwi.jt2XCi', 'chefchef@gmail.com', '5f4Ck8xhL9HLvmgdPeyuz6JbjNj5Eo2Y', 2),
(5, 'banned', 'Banned', '$2y$13$5dUsxcdcErCRh1j9ABu1T.SwCLeCBjy4HcqKKklmtnjNXBi6NSxRu', 'banned@gmail.com', 'ro6Iuw7Sfb8rVfMuL60nLmKii2-FYQe1', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categoryid` (`category`);

--
-- Indexes for table `dish_order`
--
ALTER TABLE `dish_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_approver` (`approver_id`),
  ADD KEY `fk_customer` (`customer_id`),
  ADD KEY `fk_dishid` (`dish_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dish`
--
ALTER TABLE `dish`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dish_order`
--
ALTER TABLE `dish_order`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dish`
--
ALTER TABLE `dish`
  ADD CONSTRAINT `fk_categoryid` FOREIGN KEY (`category`) REFERENCES `category` (`id`);

--
-- Constraints for table `dish_order`
--
ALTER TABLE `dish_order`
  ADD CONSTRAINT `fk_approver` FOREIGN KEY (`approver_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_customer` FOREIGN KEY (`customer_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_dishid` FOREIGN KEY (`dish_id`) REFERENCES `dish` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
